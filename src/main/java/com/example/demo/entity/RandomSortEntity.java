package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;


@Entity
@Table(name="randomsort")
public class RandomSortEntity {
	@Id
	@GeneratedValue	
	private Long id;
	@Column(name = "numberofelements")
	private int numberofelements;
	@Column(name = "originalarray")	
	private String originalarray;
	@Column(name = "sortedarray")
	private String sortedarray;

    public RandomSortEntity(int numberofelements, String originalarray, String sortedarray) {
       this.numberofelements = numberofelements;
       this.originalarray = originalarray;
       this.sortedarray = sortedarray;
    }
	
	@Override
   public String toString() {
       return "Result{" +
               "id=" + id +
               ", numberofelements='" + numberofelements + '\'' +
               ", originalarray=" + originalarray.toString() +
               ", sortedarray=" + sortedarray +
               '}';
   }
}
