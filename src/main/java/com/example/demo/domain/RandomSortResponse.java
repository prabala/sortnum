package com.example.demo.domain;

import java.io.Serializable;
import java.util.Arrays;

import org.springframework.stereotype.Component;

@Component
public class RandomSortResponse implements Serializable {
	
	
	private String noOfElements;
		
	private String originalNumbers;
	
	private String sortedNumbers;
	
	private String elapsedTime;

	public String getNoOfElements() {
        return noOfElements;
    }

    public void setNoOfElements(String noOfElements) {
        this.noOfElements = noOfElements;
    }

    public String getOriginalNumbers() {
        return originalNumbers;
    }

    public void setOriginalNumbers(String originalNumbers) {
        this.originalNumbers = originalNumbers;
    }

    public String getSortedNumbers() {
        return sortedNumbers;
    }

    public void setSortedNumbers(String sortedNumbers) {
        this.sortedNumbers = sortedNumbers;
    }
    
    public String getElapsedTime() {
        return elapsedTime;
    }

    public void setElapsedTime(String elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

	@Override
	public String toString() {
		
	       return "Result{" +               
           " noOfElements='" + noOfElements + '\'' +
           ", originalNumbers=" + originalNumbers +
           ", sortedNumbers=" + sortedNumbers +
            ", elapsedTime=" + elapsedTime + " in milliseconds" +
           '}';
   }
}
