package com.example.demo.domain;

import org.springframework.stereotype.Component;

@Component
public class RandomSortDo {
	    
    private int noOfElements;
	
	
    public int getNoOfElements() {
        return noOfElements;
    }

    public void setNoOfElements(int noOfElements) {
        this.noOfElements = noOfElements;
    }

}
