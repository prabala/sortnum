package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SortnumApplication {

	public static void main(String[] args) {
		SpringApplication.run(SortnumApplication.class, args);
	}

}
