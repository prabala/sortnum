package com.example.demo.repository;

import com.example.demo.entity.RandomSortEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RandomSortRepository extends JpaRepository<RandomSortEntity, Long> {}

