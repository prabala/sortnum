# sortnum
Spring Microservices to do random number sorting

To run the project,
1. Open STS/Eclipse IDE and run as Spring Boot App
2. Open postman, hit rest url in post method and pass json object for total number to be sorted.

rest url => http://localhost:8080/randomsort
Json object => { "noOfElements" : 18 }

3. 18 random numbers will be generated and those 18 numbers sorted using merge sort.
4. Inmemory DB h2 will be updated with total number, original array of elements, sorted array of elements and elapsed time
5. Same result will be return back as response
